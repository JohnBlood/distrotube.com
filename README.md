# DistroTube.com

![Screenshot of my desktop](https://gitlab.com/dwt1/distrotube.com/raw/master/screenshot.png) 

This repository hosts the source for the [DistroTube.com website](https://www.distrotube.com). This site was built using Hugo, which is a static site generator.  The content pages are written in markdown.  For more information, checkout the documentation for Hugo.
