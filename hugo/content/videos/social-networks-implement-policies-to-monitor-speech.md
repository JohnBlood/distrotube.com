---
title: "Social Networks Implement Policies To Monitor Speech And Thought"
image: images/thumbs/0673.jpg
date: 2020-07-20T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy", "Social Media"]
---

#### VIDEO

{{< amazon src="Social+Networks+Implement+Policies+To+Monitor+Speech+And+Thought.mp4" >}}
&nbsp;

#### SHOW NOTES

Recently, I noticed that several of the big social media sites are starting to implement new policies to help combat some of the bad, toxic people that hangout on those social networking sites.  I will share with you some of the programs that the big social site's are implementing.

REFERENCED:
+ https://www.distrotube.com/blog/social-media-sites-implement-new-policies-on-wrong-think/