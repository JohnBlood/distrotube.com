---
title: "New Linux Users Should Be Given Ubuntu Rather Than Choice"
image: images/thumbs/0581.jpg
date: 2020-03-28T12:22:40+06:00
author: Derek Taylor
tags: ["Ubuntu", ""]
---

#### VIDEO

{{< amazon src="New+Linux+Users+Should+Be+Given+Ubuntu+Rather+Than+Choice.mp4" >}}
&nbsp;

#### SHOW NOTES

So I see this has been a hot topic in the Linux community lately. And that topic is choice. And specifically the choices that we present to would-be New Linux Users. And the reason this topic has been discussed so much lately is because of an interview that Jason Evangelho of the Linux4Everyone podcast did with Alan Pope (aka. Popey).   Alan's answer was a bit controversial...

SOURCES:
+ Jason's interview with Alan - https://www.youtube.com/watch?v=_XcoWKoubjE