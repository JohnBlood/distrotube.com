---
title: "Unfettered Freedom, Ep. 11 - High Priority FSF, Linux Ransomware, Best Distros 2020, Gimp, NCoC" 
image: images/thumbs/0753.jpg
date: 2020-11-11T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/unfettered-freedom-ep-11-high-priority/75dcf9ef04be1d0fb2de90a074d579c2602cd8a6?r=76mUtNJtYB9UMWjfP3uBjk7TneDBTrQe" allowfullscreen></iframe>

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software. On this freedom-packed episode:

+ 0:00 - Intro
+ 2:03 - Committee begins review of FSF's High Priority Projects list
+ 9:14 - New ransomware threat jumps from Windows to Linux
+ 15:48 - Best Linux desktop distributions of 2020
+ 24:21 - A preview of upcoming 3.0, development release GIMP 2.99.2 is out
+ 27:53 - A Code of Conduct written for adults?
+ 34:02 - Outro and a THANK YOU to the patrons!
