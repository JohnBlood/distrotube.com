---
title: "Kubuntu 17.10 Artful Aarvark - Install and Review"
image: images/thumbs/0014.jpg
date: Tue, 17 Oct 2017 01:53:46 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Kubuntu", "KDE"]
---

#### VIDEO

{{< amazon src="Kubuntu+17.10+Artful+Aarvark+-+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

We take a look at the new Kubuntu 17.10. I review this about a day and a half early. This is the "release candidate" of Kubutnu 17.10 but nothing is changing between now and the release.
