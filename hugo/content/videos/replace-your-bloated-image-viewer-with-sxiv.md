---
title: "Replace Your Bloated Image Viewer With SXIV" 
image: images/thumbs/0777.jpg
date: 2020-12-20T12:23:40+06:00
author: Derek Taylor
tags: ["GUI Apps", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/replace-your-bloated-image-viewer-with/5d1197b42a87731b3b764d3f87fa4c84c746af22?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

Want an image viewer that is "just an image viewer"?  Want an image viewer that follows the Unix Philosophy of "do one thing and do it well"?  Want an image viewer that is Suckless-inspired?  Then sxiv (Simple X Image Viewer) is the right choice for you.

#### ERRATA:
In the video, I mentioned that 'plus' and 'minus' zoom in/out.  Then I said 'minus' flips the image vertically.  I misspoke.  That should be 'underscore'.  The 'pipe' character does flip horizontally.

#### REFERENCED:
+ https://github.com/muennich/sxiv