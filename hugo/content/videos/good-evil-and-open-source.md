---
title: "Good, Evil and Open Source"
image: images/thumbs/0493.jpg
date: 2020-01-07T12:22:40+06:00
author: Derek Taylor
tags: ["Open Source", "FOSS Philosophy"]
---

#### VIDEO

{{< amazon src="Good+Evil+And+Open+Source.mp4" >}}
&nbsp;

#### SHOW NOTES

This old debate keeps coming up.  Can we prevent "evil" people from using free and open source software for "evil" purposes?  The answer is very clear and straightforward.  But it seems that is a difficult concept for some to grasp.

REFERENCED:
+ ► https://opensource.org/osd-annotated - The Open Source Definition
+ ► https://opensource.org/faq#evil - Can You Prevent Evil People From Using Your Code?
