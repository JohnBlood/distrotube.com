---
title: "Making GNOME Tolerable in Fedora 28"
image: images/thumbs/0169.jpg
date: Thu, 05 Apr 2018 22:21:09 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Fedora", "GNOME"]
---

#### VIDEO

{{< amazon src="Making+GNOME+Tolerable+in+Fedora+28.mp4" >}}
&nbsp;

#### SHOW NOTES

Is it possible to make GNOME great again...in Fedora 28? I add a few extensions and install a couple of packages to make GNOME into a proper desktop environment. 
