---
title: "Day 7 - dwm and suckless - I Hate The Patching!"
image: images/thumbs/0341.jpg
date: Sun, 27 Jan 2019 22:38:44 +0000
author: Derek Taylor
tags: ["tiling window managers", "dwm"]
---

#### VIDEO

{{< amazon src="Day+7+dwm+and+suckless+I+Hate+The+Patching.mp4" >}}
&nbsp;

#### SHOW NOTES

It's day seven of living in dwm.   That equals one week...for you folks in Arkansas.  So how it's going for me?  Lets' discuss. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=0AmvYM_Ajd4&amp;redir_token=qrP2kVM4fgxw1rE-tN-ylXAox9d8MTU1MzYzOTk2MEAxNTUzNTUzNTYw&amp;event=video_description&amp;q=https%3A%2F%2Fsuckless.org%2F" target="_blank">https://suckless.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=0AmvYM_Ajd4&amp;redir_token=qrP2kVM4fgxw1rE-tN-ylXAox9d8MTU1MzYzOTk2MEAxNTUzNTUzNTYw&amp;event=video_description&amp;q=https%3A%2F%2Fdwm.suckless.org%2Fpatches%2F" target="_blank">https://dwm.suckless.org/patches/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=0AmvYM_Ajd4&amp;redir_token=qrP2kVM4fgxw1rE-tN-ylXAox9d8MTU1MzYzOTk2MEAxNTUzNTUzNTYw&amp;event=video_description&amp;q=https%3A%2F%2Fgitlab.com%2Fdwt1%2Fdotfiles" target="_blank">https://gitlab.com/dwt1/dotfiles
