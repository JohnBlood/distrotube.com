---
title: "Let's Share Our Favorite Bash Aliases"
image: images/thumbs/0696.jpg
date: 2020-08-18T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Lets+Share+Our+Favorite+Bash+Aliases.mp4" >}}
&nbsp;

#### SHOW NOTES

I think one of the things that most of us do not fully appreciate is how much time we can save by using shell aliases.  I don't think I use enough aliases and when I check out other people's configs, I don't think you guys use enough aliases.  So I thought I'd share some of my aliases with you guys.