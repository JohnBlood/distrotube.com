---
title: "Is Jitsi A Viable Alternative To Zoom? DT LIVE"
image: images/thumbs/0610.jpg
date: 2020-04-29T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="Is+Jitsi+A+Viable+Alternative+To+Zoom++DT+LIVE-A4xJciFfFj0.mp4" >}}
&nbsp;

#### SHOW NOTES

So I've been looking for a Zoom alternative that is worthy.     I would love to use  a free and open source solution, but none have impressed me in the past, including Jitsi.  But I thought I would give a try tonight to see if it's gotten better.  This will be a short test stream.  You can join me on the video call if you want.

REFERENCED:
+ https://jitsi.org/