---
title: "The Age of the Introvert"
image: images/thumbs/0455.jpg
date: Sat, 02 Nov 2019 00:06:00 +0000
author: Derek Taylor
tags: ["memes", ""]
---

#### VIDEO

{{< amazon src="The+Age+of+the+Introvert.mp4" >}}
&nbsp;

#### SHOW NOTES

Most computer geeks are introverted. We are socially inept and awkward individuals who may or may not also be autistic and/or schizophrenic. But is this an advantage in today's world?

#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=Fz2kNc4oC4wCqU22MsKyTw0igB98MTU3NzQ5MTU5OEAxNTc3NDA1MTk4&amp;v=ge8XcDGH9mY&amp;q=https%3A%2F%2Fwww.urbandictionary.com%2Fdefine.php%3Fterm%3DIntrovert&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.urbandictionary.com/defin...
