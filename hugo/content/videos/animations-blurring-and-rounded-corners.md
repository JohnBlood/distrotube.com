---
title: "Animations, Blurring and Rounded Corners For Your Window Manager" 
image: images/thumbs/0769.jpg
date: 2020-12-07T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/animations-blurring-and-rounded-corners/10eb43975eb59c7919fb5c888974157f5a86847d?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

One of the things that people love about some of the big desktop environments are the cool window animations, the blurring effects and the option to have rounded corners.  But just because you use a standalone window manager does not mean you cannot have these cool effects.  Check out this fork of picom!

REFERENCED:
+ https://github.com/jonaburg/picom