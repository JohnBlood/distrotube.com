---
title: "Want To Switch To Linux But Need Notepad++?" 
image: images/thumbs/0709.jpg
date: 2020-09-08T12:23:40+06:00
author: Derek Taylor
tags: ["Windows", ""]
---

#### VIDEO

{{< amazon src="Want+To+Switch+To+Linux+But+Need+Notepad%2B%2B.mp4" >}}
&nbsp;

#### SHOW NOTES

I keep getting comments from viewers of the channel saying that they would like to switch from Windows to Linux, but they can't switch because they need Notepad++.  Now this is a strange comment to me considering how many text editors that we have on Linux.  But, I will show you how to get Notepad++ installed on Linux, and I will also show you a native Linux text editor that mimics Notepad++.

REFERENCED:
+ https://notepad-plus-plus.org/ - Notepad++
+ https://www.winehq.org/ - Wine
+ https://notepadqq.com/ - Notepadqq