---
title: "Some Useful And Some Not So Useful X11 Apps"
image: images/thumbs/0600.jpg
date: 2020-04-18T12:23:40+06:00
author: Derek Taylor
tags: ["GUI Apps", "CLI Apps"]
---

#### VIDEO

{{< amazon src="Some+Useful+And+Some+Not-So-Useful+X11+Apps.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I am going to briefly show you a few X11 utilities that you may or may not know about.  Some are very useful, some are not so useful, and some are just plain goofy.

REFERENCED:
+ xprop
+ xset
+ xcalc
+ xclock
+ xedit
+ xeyes
+ xfishtank
+ xkill
+ xload
+ xscreensaver
+ xterm
+ xvkbd
+ xzoom