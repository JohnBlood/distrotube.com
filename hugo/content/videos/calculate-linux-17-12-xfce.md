---
title: "Calculate Linux 17.12 XFCE - Great Way To End 2017"
image: images/thumbs/0076.jpg
date: Sun, 31 Dec 2017 16:03:06 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Calculate Linux", "XFCE"]
---

#### VIDEO

{{< amazon src="Calculate+Linux+17.12+XFCE+-+Great+Way+To+End+2017.mp4" >}}  
&nbsp;

#### SHOW NOTES

I take a look at the recently released Calculate Linux 17.12. I reviewed Calculate Linux a couple of months ago (an older snapshop that used KDE). This snapshot of Calculate was released today. I chose to look at the XFCE version this time. Happy New Year! <a href="http://www.calculate-linux.org/">http://www.calculate-linux.org/</a>
