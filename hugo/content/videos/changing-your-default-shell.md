---
title: "Changing Your Default Shell"
image: images/thumbs/0386.jpg
date: Wed, 01 May 2019 03:02:33 +0000
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Changing+Your+Default+Shell.mp4" >}}
&nbsp;

#### SHOW NOTES

Wondering how to change your default shell?  I will show you three easy ways to do just that. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Flinux.die.net%2Fman%2F1%2Fchsh&amp;redir_token=aZzoqW9Y7ltil_J4S0-3ZoKWBf18MTU1NzE5ODE1NEAxNTU3MTExNzU0&amp;event=video_description&amp;v=BwFlp2b9MPU" target="_blank">https://linux.die.net/man/1/chsh

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Flinux.die.net%2Fman%2F8%2Fusermod&amp;redir_token=aZzoqW9Y7ltil_J4S0-3ZoKWBf18MTU1NzE5ODE1NEAxNTU3MTExNzU0&amp;event=video_description&amp;v=BwFlp2b9MPU" target="_blank">https://linux.die.net/man/8/usermod

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.cyberciti.biz%2Ffaq%2Funderstanding-etcpasswd-file-format%2F&amp;redir_token=aZzoqW9Y7ltil_J4S0-3ZoKWBf18MTU1NzE5ODE1NEAxNTU3MTExNzU0&amp;event=video_description&amp;v=BwFlp2b9MPU" target="_blank">https://www.cyberciti.biz/faq/underst...
