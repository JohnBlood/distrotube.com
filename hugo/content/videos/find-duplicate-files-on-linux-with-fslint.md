---
title: "Find Duplicate Files On Linux With FSlint"
image: images/thumbs/0589.jpg
date: 2020-04-05T12:23:40+06:00
author: Derek Taylor
tags: ["GUI Apps", ""]
---

#### VIDEO

{{< amazon src="Find+Duplicate+Files+On+Linux+With+FSlint.mp4" >}}
&nbsp;

#### SHOW NOTES

Find duplicate files in your file system and delete them with FSlint.  It is a free and open source GUI utility that can be found in your distro's repositories.

REFERENCED:
+ https://github.com/pixelb/fslint