---
title: "Calculate Linux 17.12.2 MATE Install & Review"
image: images/thumbs/0123.jpg
date: Sat, 24 Feb 2018 21:07:40 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Calculate Linux"]
---

#### VIDEO

{{< amazon src="Calculate+Linux+17+12+2+MATE+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video I take a look at the recently released Calculate Linux 17.12.2 MATE edition. Calculate is a Gentoo-based distro that aims to be user-friendly and easy-to-install. https://www.calculate-linux.org/
