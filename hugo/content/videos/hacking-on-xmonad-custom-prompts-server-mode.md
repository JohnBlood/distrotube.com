---
title: "Hacking On Xmonad - Custom Prompts And Server Mode"
image: images/thumbs/0641.jpg
date: 2020-06-06T12:23:40+06:00
author: Derek Taylor
tags: ["tiling window managers", "xmonad"]
---

#### VIDEO

{{< amazon src="Hacking+On+Xmonad+Custom+Prompts+And+Server+Mode.mp4" >}}
&nbsp;

#### SHOW NOTES

I've had some people asking if I could look into how to create custom prompts in Xmonad.  I've also had people asking if I could look into Server Mode in Xmonad.  The reason?  It's because these aren't that easy to get working and you won't find that much in terms of documentation.  So let's see if I can make sense out of this.