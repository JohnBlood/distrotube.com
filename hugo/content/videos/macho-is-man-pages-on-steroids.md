---
title: "Macho Is Man Pages On Steroids"
image: images/thumbs/0666.jpg
date: 2020-07-11T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Macho+Is+Man+Pages+On+Steroids.mp4" >}}
&nbsp;

#### SHOW NOTES

I came across this fantastic blog post about a shell script called "macho" that searches for manpages.  It uses a variety of shell commands including: man, apropos, grep, sed, awk, sort and fzf.  You can also use it in conjunction with a launcher like dmenu or rofi.

REFERENCED:
+ https://hiphish.github.io/blog/2020/05/31/macho-man-command-on-steroids/ - HiPhish's Article About Macho