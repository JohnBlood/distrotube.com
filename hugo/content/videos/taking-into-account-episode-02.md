---
title: "Taking Into Account, Ep. 2 - Lubuntu, Windows DaaS, Firefox, Steam"
image: images/thumbs/0253.jpg
date: Thu, 02 Aug 2018 22:29:04 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+2+-+Lubuntu%2C+Windows+DaaS%2C+Firefox%2C+Steam.mp4" >}}
&nbsp;

#### SHOW NOTES

+ 0:30 Lubuntu shifting focus from older computers 
+ 4:55 Windows offers Desktop as a Service 
+ 10:15 New website that helps you choose your distro 
+ 17:08 Firefox is getting a new logo 
+ 21:35 Malware found on Steam 
+ 26:20 I read a question from one of the viewers
