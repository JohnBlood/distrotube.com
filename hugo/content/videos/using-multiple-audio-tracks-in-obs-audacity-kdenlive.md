---
title: "Using Multiple Audio Tracks In OBS, Audacity and Kdenlive"
image: images/thumbs/0688.jpg
date: 2020-08-07T12:23:40+06:00
author: Derek Taylor
tags: ["Audio", "Video"]
---

#### VIDEO

{{< amazon src="Using+Multiple+Audio+Tracks+In+OBS%2C+Audacity+and+Kdenlive.mp4" >}}
&nbsp;

#### SHOW NOTES

Are you a video content creator or a podcaster?  Are you wanting to record your audio sources as individual separate tracks?  I will show you how to do this using OBS.  Also, I will show you how Audacity and Kdenlive handle files with multiple audio tracks.