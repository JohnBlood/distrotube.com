---
title: "OpenSUSE Tumbleweed Install and Review KDE edition"
image: images/thumbs/0012.jpg
date: Mon, 16 Oct 2017 01:51:54 +0000
author: Derek Taylor
tags: ["Distro Reviews", "OpenSUSE", "KDE"]
---

#### VIDEO

{{< amazon src="OpenSUSE+Tumbleweed+Install+and+Review+KDE+edition.mp4" >}}  
&nbsp;

#### SHOW NOTES

I just can't stop installing rolling release ditros. Today I install OpenSUSE Tumbleweed which is OpenSUSE's rolling release version.
