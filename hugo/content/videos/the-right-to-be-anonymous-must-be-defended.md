---
title: "The Right To Being Anonymous Online Must Be Defended"
image: images/thumbs/0669.jpg
date: 2020-07-15T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy"]
---

#### VIDEO

{{< amazon src="The+Right+To+Being+Anonymous+Online+Must+Be+Defended.mp4" >}}
&nbsp;

#### SHOW NOTES

There are two groups of people. Those that value their privacy; and those that do not.  That second group of people is dangerous, especially those that to diminish or prevent the rights of people to be anonymous on the Internet.  What's even more disturbing is some of the people in that second group claim to be champions of the Free Software movement.