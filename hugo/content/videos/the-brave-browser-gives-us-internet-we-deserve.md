---
title: "The Brave Browser Gives Us Internet We Deserve"
image: images/thumbs/0513.jpg
date: 2020-01-13T17:37:59-06:00
author: Derek Taylor
tags: ["Brave", "App Reviews"]
---

#### VIDEO

{{< amazon src="The+Brave+Browser+Gives+Us+The+Internet+We+Deserve.mp4" >}}
&nbsp;

#### SHOW NOTES

The Brave web browser is a free and open source browser that strips all advertisements from websites.  It allows you to earn money while browsing which you can use to tip your favorite content creators.  And Brave is privacy-focused!  

REFERENCED:
+ ► https://brave.com/
