---
title: "Man Pages Got Your Panties In A Twist? Use Woman Instead!"
image: images/thumbs/0686.jpg
date: 2020-08-03T12:23:40+06:00
author: Derek Taylor
tags: ["Emacs", "command line"]
---

#### VIDEO

{{< amazon src="Man+Pages+Got+Your+Panties+In+A+Twist+Use+Woman+Instead.mp4" >}}
&nbsp;

#### SHOW NOTES

In the last few months, it seems everyone has suddenly decided that they need to be perceived as "woke" in regards to social injustices, including sexism and gender equality.  One of the sticking points for the Linux community has been our beloved "man" pages.  Why does it have to be "man"?  Well, in Emacs...it doesn't!

REFERENCED:
+ https://www.gnu.org/software/emacs/manual/html_mono/woman.html