---
title: "Transparency With The Compton Compositor"
image: images/thumbs/0462.jpg
date: Sun, 10 Nov 2019 00:16:00 +0000
author: Derek Taylor
tags: ["tiling window managers", "terminal"]
---

#### VIDEO

{{< amazon src="Transparency+With+The+Compton+Compositor.mp4" >}}
&nbsp;

#### SHOW NOTES

A common question I get is "how did you get transparency in (program name)?" This can be especially tricky when using standalone window managers that do not include a builtin compositor. You have to use a third party compositor. The one I choose to run is compton.

#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgithub.com%2Fchjj%2Fcompton&amp;redir_token=MYJbTmmSUDSPSxNCmqSBY41RPoh8MTU3NzQ5MjIwNUAxNTc3NDA1ODA1&amp;v=3OUzJs3aCBw&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://github.com/chjj/compton
