---
title: "Use AppImages To De-Bloat Your System" 
image: images/thumbs/0770.jpg
date: 2020-12-08T12:23:40+06:00
author: Derek Taylor
tags: ["AppImages", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/use-appimages-to-de-bloat-your-system/622101129b8a291328fa011af1f2b0ea31bf2537?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

I have noticed that my Arco Linux installation has become very bloated as far as the number of packages installed (nearly 2,000!).  So I was looking for a way to de-bloat my system.  I think I have found the solution--AppImages!

REFERENCED:
+ https://www.appimagehub.com/ - AppImageHub