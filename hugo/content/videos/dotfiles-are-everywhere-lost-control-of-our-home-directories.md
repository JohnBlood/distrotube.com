---
title: "Dotfiles Are Everywhere. We've Lost Control Of Our Home Directories!"
image: images/thumbs/0345.jpg
date: Sat, 02 Feb 2019 22:45:11 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Dotfiles+Are+Everywhere+Weve+Lost+Control+Of+Our+Home+Directories.mp4" >}}
&nbsp;

#### SHOW NOTES

This situation has gotten out of hand.  Programs are placing dotfiles  where they want rather than placing them according to the XDG Base  Directory  Specification.   

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=AFtfpluqv14&amp;event=video_description&amp;redir_token=uxSJFJ_n6fz4WConsgLyeo8hmPd8MTU1MzY0MDMyMkAxNTUzNTUzOTIy&amp;q=https%3A%2F%2F0x46.net%2Fthoughts%2F2019%2F02%2F01%2Fdotfile-madness%2F" target="_blank">https://0x46.net/thoughts/2019/02/01/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=AFtfpluqv14&amp;event=video_description&amp;redir_token=uxSJFJ_n6fz4WConsgLyeo8hmPd8MTU1MzY0MDMyMkAxNTUzNTUzOTIy&amp;q=https%3A%2F%2Fspecifications.freedesktop.org%2Fbasedir-spec%2Fbasedir-spec-latest.html" target="_blank">https://specifications.freedesktop.or...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=AFtfpluqv14&amp;event=video_description&amp;redir_token=uxSJFJ_n6fz4WConsgLyeo8hmPd8MTU1MzY0MDMyMkAxNTUzNTUzOTIy&amp;q=https%3A%2F%2Fwiki.archlinux.org%2Findex.php%2FXDG_Base_Directory" target="_blank">https://wiki.archlinux.org/index.php/...
