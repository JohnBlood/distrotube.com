---
title: "Mabox Linux 17.02 First Impression Install & Review"
image: images/thumbs/0087.jpg
date: Sat, 13 Jan 2018 01:16:20 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Mabox Linux"]
---

#### VIDEO

{{< amazon src="Mabox+Linux+17.02+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video I take a quick look at a Manjaro-based Linux distro that features the Openbox window manager. That Linux distro is called Mabox. Is this the distro that will end up replacing Manjaro KDE on my main machine in the near future? <a href="https://maboxlinux.org/">https://maboxlinux.org/</a>
