---
title: "Canto - Terminal-Based RSS Feed Reader"
image: images/thumbs/0030.jpg
date: Sun, 05 Nov 2017 02:35:23 +0000
author: Derek Taylor
tags: ["TUI Apps", "canto", "RSS"]
---

#### VIDEO

{{< amazon src="Canto+-+Terminal-Based+RSS+Feed+Reader.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video I review a text-based application called Canto.. It is a great little RSS news reader that runs from the terminal. Lightweight, fast and configurable!
