---
title: "Blue Yeti Blackout Edition USB Microphone on Linux"
image: images/thumbs/0099.jpg
date: Fri, 26 Jan 2018 01:27:38 +0000
author: Derek Taylor
tags: ["Audio", ""]
---

#### VIDEO

{{< amazon src="Blue+Yeti+Blackout+Edition+USB+Microphone+on+Linux.mp4" >}}  
&nbsp;

#### SHOW NOTES

My 100th video is a quick look at a great microphone I just picked up--the Blue Yeti Blackout Edition USB microphone. How does it work in Linux? Just dandy! 
