---
title: "Taking Into Account, Ep. 28 - Open Source, GNOME panel, SpeakUp, Shellbot, New User Distros, Apps"
image: images/thumbs/0348.jpg
date: Thu, 07 Feb 2019 22:48:58 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+28.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=nkXXTmmo82o&amp;t=43s">0:43 Umpires of open source licenses. Who decides whether something is open source or not? 

<a href="https://www.youtube.com/watch?v=nkXXTmmo82o&amp;t=426s">7:06 Back in black?  GNOME to remove the translucent top panel effect in future releases. 

<a href="https://www.youtube.com/watch?v=nkXXTmmo82o&amp;t=603s">10:03 Security researchers discover two new Linux backdoors--SpeakUp  and Shellbot. 

<a href="https://www.youtube.com/watch?v=nkXXTmmo82o&amp;t=834s">13:54 What are the top five Linux distros for the new user?  Will you agree with this writer's top five? 

<a href="https://www.youtube.com/watch?v=nkXXTmmo82o&amp;t=1306s">21:46 New app releases:  LibreOffice 6.2, Flowblade 2.0 and Green With Envy 0.10. 

<a href="https://www.youtube.com/watch?v=nkXXTmmo82o&amp;t=1568s">26:08 Earlier in the week, I made a video about DistroWatch dropping Mageia from it's "Major Distros" page... 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fopensource.com%2Farticle%2F19%2F2%2Fumpires-open-source-licenses&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://opensource.com/article/19/2/u...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fopensource.org%2Fosd-annotated&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://opensource.org/osd-annotated

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F02%2Fgnome-shell-translucent-top-panel-effect-removed&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://www.omgubuntu.co.uk/2019/02/g...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fnews.softpedia.com%2Fnews%2Fgnome-3-32-desktop-environment-enters-beta-final-release-arrives-march-13th-524859.shtml&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://news.softpedia.com/news/gnome...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fsecurity-researchers-discover-new-linux-backdoor-named-speakup%2F&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://www.zdnet.com/article/securit...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Foutlaws-shellbot-infects-servers-for-monero-mining%2F&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://www.zdnet.com/article/outlaws...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.linux.com%2Fblog%2Flearn%2F2019%2F2%2Ftop-5-linux-distributions-new-users&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://www.linux.com/blog/learn/2019...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Ffossbytes.com%2Flibreoffice-6-2-released-features-download%2F&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://fossbytes.com/libreoffice-6-2...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fitsfoss.com%2Fflowblade-video-editor-release%2F&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://itsfoss.com/flowblade-video-e...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F02%2Feasily-overclock-nvidia-gpu-on-linux-with-this-new-app&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://www.omgubuntu.co.uk/2019/02/e...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fdistrowatch.com%2Fdwres.php%3Fresource%3Dmajor&amp;redir_token=doqdNCRB3xDqWzHdXadHXSNVYfl8MTU1MzY0MDU0MkAxNTUzNTU0MTQy&amp;event=video_description&amp;v=nkXXTmmo82o" target="_blank">https://distrowatch.com/dwres.php?res...
