---
title: "Switching from KDE back to Openbox. And a channel milestone!"
image: images/thumbs/0095.jpg
date: Mon, 22 Jan 2018 01:21:55 +0000
author: Derek Taylor
tags: ["KDE", "Openbox"]
---

#### VIDEO

{{< amazon src="Switching+from+KDE+back+to+Openbox.+And+a+channel+milestone!.mp4" >}}  
&nbsp;

#### SHOW NOTES

A quick video discussing why I am leaving KDE behind and running back to Openbox.  Also, I wanted to sincerely thank you guys for helping this channel grow as quickly as it has.  Much appreciated!
