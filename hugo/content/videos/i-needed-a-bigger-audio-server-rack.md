---
title: "I Needed A Bigger Audio Server Rack"
image: images/thumbs/0681.jpg
date: 2020-07-28T12:23:40+06:00
author: Derek Taylor
tags: ["Linux Audio", ""]
---

#### VIDEO

{{< amazon src="I+Needed+A+Bigger+Audio+Server+Rack.mp4" >}}
&nbsp;

#### SHOW NOTES

When I decided to start purchasing audio equipment, I bought a 12U server rack.  I filled up that 12U rack as soon as a I bought it.  So I wanted to buy more audio equipment but I needed a bigger rack.  So I pulled out the credit card, opened up Amazon in my web browser, and I went shopping!

The New Equipment:
+  https://amzn.to/3faBeYB - Raising Electronics 27U Rolling Rack
+  https://amzn.to/304HQTY - Penn Elcom 1U LED Light
+  https://amzn.to/3jNEQTV - AC Infinity 2U Drawers
+  https://amzn.to/3jNuaEF - Navepoint Server Shelf
+  https://amzn.to/30WkQ8X - Behringer Exciter (SX3040 V2)

Audio Equipment I Already Had:
+  https://amzn.to/332lu7F - dbx 231s Equalizer
+  https://amzn.to/2BAZK7o - dbx 166xs Compressor/Limiter/Gate
+  https://amzn.to/2X310Yu - Mackie PROFX12V2 Mixer
+  https://amzn.to/2CVZpgl - Rackmount Bracket for Mackie PROFX12V2
+  https://amzn.to/30UzcH5 - Furman Power Conditioner