---
title: "Taking Into Account, Ep. 32 - Intel CPUs, Windows Calculator, Ubuntu Studio, Mint, Good vs Evil"
image: images/thumbs/0362.jpg
date: Thu, 07 Mar 2019 23:09:52 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+32.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=Gwgpo6xPOJ4&amp;t=48s">0:48 Intel CPUs afflicted with simple data-spewing spec-exec vulnerability. 

<a href="https://www.youtube.com/watch?v=Gwgpo6xPOJ4&amp;t=338s">5:38 Microsoft open-sources its Windows calculator on GitHub. 

<a href="https://www.youtube.com/watch?v=Gwgpo6xPOJ4&amp;t=671s">11:11 Ubuntu Studio’s status as an official flavor of Ubuntu was recently called into question. 

<a href="https://www.youtube.com/watch?v=Gwgpo6xPOJ4&amp;t=938s">15:38 Linux Mint's website was looking "old".  So they have unveiled a new logo and website design. 

<a href="https://www.youtube.com/watch?v=Gwgpo6xPOJ4&amp;t=1196s">19:56 Linux 5.0 is out.  A major milestone with minor improvements.  

<a href="https://www.youtube.com/watch?v=Gwgpo6xPOJ4&amp;t=1429s">23:49 I read a viewer question regarding "evil" software. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Gwgpo6xPOJ4&amp;q=https%3A%2F%2Fwww.theregister.co.uk%2F2019%2F03%2F05%2Fspoiler_intel_processor_flaw%2F&amp;redir_token=1HbnU7_ZaDpmj9qwSKTiTiK6asd8MTU1MzY0MTc5NUAxNTUzNTU1Mzk1" target="_blank">https://www.theregister.co.uk/2019/03...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Gwgpo6xPOJ4&amp;q=https%3A%2F%2Fbetanews.com%2F2019%2F03%2F06%2Fmicrosoft-windows-calculator-open-source-github%2F&amp;redir_token=1HbnU7_ZaDpmj9qwSKTiTiK6asd8MTU1MzY0MTc5NUAxNTUzNTU1Mzk1" target="_blank">https://betanews.com/2019/03/06/micro...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Gwgpo6xPOJ4&amp;q=https%3A%2F%2Fgithub.com%2FMicrosoft%2Fcalculator&amp;redir_token=1HbnU7_ZaDpmj9qwSKTiTiK6asd8MTU1MzY0MTc5NUAxNTUzNTU1Mzk1" target="_blank">https://github.com/Microsoft/calculator

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Gwgpo6xPOJ4&amp;q=https%3A%2F%2Fubuntustudio.org%2F2019%2F03%2Fstatement-to-the-community%2F&amp;redir_token=1HbnU7_ZaDpmj9qwSKTiTiK6asd8MTU1MzY0MTc5NUAxNTUzNTU1Mzk1" target="_blank">https://ubuntustudio.org/2019/03/stat...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Gwgpo6xPOJ4&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F03%2Flinux-mint-is-working-on-a-new-logo&amp;redir_token=1HbnU7_ZaDpmj9qwSKTiTiK6asd8MTU1MzY0MTc5NUAxNTUzNTU1Mzk1" target="_blank">https://www.omgubuntu.co.uk/2019/03/l...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Gwgpo6xPOJ4&amp;q=https%3A%2F%2Fblog.linuxmint.com%2F%3Fp%3D3732&amp;redir_token=1HbnU7_ZaDpmj9qwSKTiTiK6asd8MTU1MzY0MTc5NUAxNTUzNTU1Mzk1" target="_blank">https://blog.linuxmint.com/?p=3732

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=Gwgpo6xPOJ4&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Flinux-5-0-a-major-milestone-with-minor-improvements%2F&amp;redir_token=1HbnU7_ZaDpmj9qwSKTiTiK6asd8MTU1MzY0MTc5NUAxNTUzNTU1Mzk1" target="_blank">https://www.zdnet.com/article/linux-5...
