---
title: "Sabayon MATE Daily Snapshot Mar 1, 2018"
image: images/thumbs/0129.jpg
date: Thu, 01 Mar 2018 21:16:39 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Sabayon", "MATE"]
---

#### VIDEO

{{< amazon src="Sabayon+MATE+Daily+Snapshot+Mar+1%2C+2018.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I do a quick install and overview of Sabayon's MATE edition. This is today's daily snapshot of Sabayon MATE. It is attractive, lightweight and comes with a nice suite of programs out-of-the-box. Sabayon is a Gentoo-based Linux distribution. <a href="https://www.sabayon.org/">https://www.sabayon.org/</a>
