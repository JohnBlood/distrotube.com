---
title: "Calcurse - Organizer and Scheduling App"
image: images/thumbs/0020.jpg
date: Sun, 22 Oct 2017 02:06:41 +0000
author: Derek Taylor
tags: ["TUI Apps", "Calcurse"]
---

#### VIDEO

{{< amazon src="Calcurse+-+Organizer+and+Scheduling+App.mp4" >}}  
&nbsp;

#### SHOW NOTES

I look at calcurse, a command line note program and appointment organizer. It is lightweight and fast and sports a user-friendly curses interface. Calcurse - http://calcurse.org/
