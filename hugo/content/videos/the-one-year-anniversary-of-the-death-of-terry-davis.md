---
title: "The One Year Anniversary of the Death of Terry Davis"
image: images/thumbs/0430.jpg
date: Sun, 11 Aug 2019 23:18:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="The+One+Year+Anniversary+of+the+Death+of+Terry+Davis.mp4" >}}
&nbsp;

#### SHOW NOTES

Today marks the one year anniversary of the death of Terry A. Davis, the creator of TempleOS. He was a very interesting and colorful person who was very popular in the tech community due to his uncontrollable outbursts on web forums and social media. Terry suffered from schizophrenia and was homeless for a time. I wanted to make this video as a tribute to Terry and to highlight the need for more funding and research into mental illness.


DONATE TO THESE ORGANIZATIONS IF YOU CAN:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=uImBTd2HpFM&amp;redir_token=nfBV-9xyA-BoYh4UvKIpj4Fxd3l8MTU3NzQ4ODcwM0AxNTc3NDAyMzAz&amp;q=https%3A%2F%2Fwww.bbrfoundation.org%2F&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.bbrfoundation.org/
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=uImBTd2HpFM&amp;redir_token=nfBV-9xyA-BoYh4UvKIpj4Fxd3l8MTU3NzQ4ODcwM0AxNTc3NDAyMzAz&amp;q=https%3A%2F%2Fwww.nami.org%2F&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://www.nami.org/


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=uImBTd2HpFM&amp;redir_token=nfBV-9xyA-BoYh4UvKIpj4Fxd3l8MTU3NzQ4ODcwM0AxNTc3NDAyMzAz&amp;q=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FTerry_A._Davis&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://en.wikipedia.org/wiki/Terry_A...
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=uImBTd2HpFM&amp;redir_token=nfBV-9xyA-BoYh4UvKIpj4Fxd3l8MTU3NzQ4ODcwM0AxNTc3NDAyMzAz&amp;q=https%3A%2F%2Ftempleos.org%2F&amp;event=video_description" target="_blank" rel="nofollow noopener noreferrer">https://templeos.org/
