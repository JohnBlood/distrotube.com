---
title: "Termonad Is A Terminal Emulator Configurable In Haskell"
image: images/thumbs/0630.jpg
date: 2020-05-24T12:23:40+06:00
author: Derek Taylor
tags: ["command line", "terminal"]
---

#### VIDEO

{{< amazon src="Termonad+Is+A+Terminal+Emulator+Configurable+In+Haskell.mp4" >}}
&nbsp;

#### SHOW NOTES

Termonad is a terminal emulator that is interesting because it can be configured using Haskell.  So it's perfect for those of you that want to practice Haskell or already have other Haskell programs installed (like Xmonad or Pandoc). 
NOTE:
Although I focused on the option to configure Termonad via Haskell, you don't have to do this if you aren't comfortable with programming.  Termonad has a preferences menu (like most terminal emulators) where you can set some options, but you are limited in what you can do using that method.  Although it's technically for power users, creating a termonad.hs is the better way to go here.

REFERENCED:
+ https://github.com/cdepillabout/termonad