---
title: "Searching For The Right Minimal Browser"
image: images/thumbs/0588.jpg
date: 2020-04-04T12:23:40+06:00
author: Derek Taylor
tags: ["Web Browsers", ""]
---

#### VIDEO

{{< amazon src="Searching+For+The+Right+Minimal+Browser.mp4" >}}
&nbsp;

#### SHOW NOTES

So I have been wanting to switch away from the big, bloated, full-featured web browsers to something more minimal.  Something with fewer features, less clutter on the screen, less distractions, etc.  Here are a few browsers that I might try out.

#### ERRATA:
I mistakenly said that Qutebrowser uses the Webkit engine.  Qutebrowser, by default, uses the Chromium engine.  But it does have the ability to use Webkit instead, if your prefer.

#### REFERENCED:
+ https://surf.suckless.org/
+ https://qutebrowser.org/
+ https://fanglingsu.github.io/vimb/
+ https://minbrowser.github.io/min/