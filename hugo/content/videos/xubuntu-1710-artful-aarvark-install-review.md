---
title: "Xubuntu 17.10 Artful Aarvark - Install and Review"
image: images/thumbs/0015.jpg
date: Wed, 18 Oct 2017 01:54:44 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Xubuntu", "XFCE"]
---

#### VIDEO

{{< amazon src="Xubuntu+17.10+Artful+Aarvark+-+Install+and+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I review the newest version of Xubuntu--17.10 codenamed Artful Aardvark. Sporting a super-fast installation process and the lightning fast XFCE desktop environment, Xubuntu 17.10 is sure to be a big hit for those running older computers or for those with newer machines but still wanting speed and minimalism.

