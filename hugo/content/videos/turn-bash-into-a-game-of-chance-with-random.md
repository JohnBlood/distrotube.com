---
title: "Turn Bash Into A Game Of Chance With $RANDOM"
image: images/thumbs/0640.jpg
date: 2020-06-05T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Turn+Bash+Into+A+Game+Of+Chance+With+RANDOM.mp4" >}}
&nbsp;

#### SHOW NOTES

This short video demonstrates how you can make the terminal a bit more exciting by introducing a bit of randomness into your commands. 