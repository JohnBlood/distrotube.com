---
title: "Are Richard Stallman and Linus Torvalds Good for Linux?"
image: images/thumbs/0436.jpg
date: Fri, 30 Aug 2019 23:34:00 +0000
author: Derek Taylor
tags: ["Richard Stallman", "Linus Torvalds"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/are-richard-stallman-and-linus-torvalds/68b26b8662eeda03091255655740b50fe7ea5dd5?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Linux users often wonder why Linux and free and open source software see such slow adoption from the consumer market. It might have something to do with the people that we are putting out front on this.
