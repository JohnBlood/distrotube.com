---
title: "BREAKING NEWS - Antergos Linux Project Ends - What Are Your Options?"
image: images/thumbs/0399.jpg
date: Tue, 21 May 2019 14:43:37 +0000
author: Derek Taylor
tags: ["Antergos", "Live Stream"]
---

#### VIDEO

{{< amazon src="BREAKING+NEWS+-+Antergos+Linux+Project+Ends+-+What+Are+Your+Options-VKPyhRVxUvU.mp4" >}}
&nbsp;

#### SHOW NOTES

Antergos has announced the end of its development. Is this a good thing? A bad thing? What about if you are an Antergos user?

&nbsp;
#### REFERENCED:
+ https://antergos.com/blog/antergos-linux-project-ends/
