---
title: "Unfettered Freedom, Ep. 5 - Blender, Thunderbird, 2FA, Linux Apps, Hurricane Laura" 
image: images/thumbs/0704.jpg
date: 2020-09-02T12:23:40+06:00
author: Derek Taylor
tags: ["Unfettered Freedom", ""]
---

#### VIDEO

{{< amazon src="Unfettered+Freedom+Ep+5.mp4" >}}
&nbsp;

#### SHOW NOTES

Unfettered Freedom is a video podcast that focuses on news and topics about GNU/Linux, free software and open source software.  On this freedom-packed episode:

+ 0:00 - Intro
+ 2:00 - Blender 2.90 is an impressive release.
+ 6:54 - Thunderbird now has OpenPGP.
+ 10:06 - Open source 2FA application.
+ 14:12 - Arch Wiki's "List of Applications" page.
+ 16:57 - Hurricane Laura, the loss of power, and lessons learned.
+ 31:05 - Outro and a THANK YOU to the patrons!