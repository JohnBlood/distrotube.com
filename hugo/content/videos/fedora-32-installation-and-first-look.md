---
title: "Fedora 32 Installation and First Look"
image: images/thumbs/0611.jpg
date: 2020-05-01T12:23:40+06:00
author: Derek Taylor
tags: ["Fedora", "GNOME"]
---

#### VIDEO

{{< amazon src="Fedora+32+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Fedora 32 was recently released so I thought I would take it for a quick spin in a virtual machine.  I run through the installation and take a brief look at Fedora with the GNOME desktop.

REFERENCED:
+ https://fedoramagazine.org/announcing-fedora-32/