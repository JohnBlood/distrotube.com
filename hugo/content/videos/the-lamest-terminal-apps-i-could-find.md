---
title: "The Lamest Terminal Apps That I Could Find" 
image: images/thumbs/0714.jpg
date: 2020-09-15T12:23:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="The+Lamest+Terminal+Apps+That+I+Could+Find.mp4" >}}
&nbsp;

#### SHOW NOTES

So I was searching through the Snap Store for interesting terminal applications.  Instead, I kept coming across really lame and corny terminal apps.  Some were so lame that I just had to install them and check them out.

REFERENCED:
+ https://snapcraft.io/cornyjokes
+ https://snapcraft.io/whatami
+ https://snapcraft.io/magicraminstaller