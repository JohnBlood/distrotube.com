---
title: "My Manjaro KDE Journey, After Five Days"
image: images/thumbs/0078.jpg
date: Mon, 01 Jan 2018 00:55:50 +0000
author: Derek Taylor
tags: ["Manjaro", "KDE"]
---

#### VIDEO

{{< amazon src="My+Manjaro+KDE+Journey%2C+After+Five+Days.mp4" >}}  
&nbsp;

#### SHOW NOTES

It's been five days since I installed Manjaro KDE on my main production machine at home. This is a quick update on how things are going for me. Check out my previous video My Manjaro KDE Journey, Day One as I reference a few things from that video. Happy New Year!
