---
title: "Taking Into Account, Ep. 23 - Firefox ads, Snaps, Kernel commits, Bloodstained, Chromebooks, i3"
image: images/thumbs/0327.jpg
date: Thu, 03 Jan 2019 19:52:06 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep+23.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=1X3Md5GMdss&amp;t=55s">0:55 Firefox now serving ads?  Mozilla says it's just an experiment. 

<a href="https://www.youtube.com/watch?v=1X3Md5GMdss&amp;t=431s">7:11 The top snap packages in the Snap Store in 2018. 

<a href="https://www.youtube.com/watch?v=1X3Md5GMdss&amp;t=821s">13:41 The Linux kernel had 75,000 commits in 2018. 

<a href="https://www.youtube.com/watch?v=1X3Md5GMdss&amp;t=1079s">17:59 Game Bloodstained promised to support Linux but changed their mind. 

<a href="https://www.youtube.com/watch?v=1X3Md5GMdss&amp;t=1399s">23:19 Google will soon let you run Windows 10 on high-end Chromebooks. 

<a href="https://www.youtube.com/watch?v=1X3Md5GMdss&amp;t=1687s">28:07 I had a few people that did not understand what I meant about my issue with i3 and "swapping workspaces." 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=1X3Md5GMdss&amp;redir_token=CbvFDWG2vMqEakSNLLwpLj19SdR8MTU1MzU0MzUzNUAxNTUzNDU3MTM1&amp;q=https%3A%2F%2Fventurebeat.com%2F2018%2F12%2F31%2Fmozilla-ad-on-firefoxs-new-tab-page-was-just-another-experiment%2F&amp;event=video_description" target="_blank">https://venturebeat.com/2018/12/31/mo...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=1X3Md5GMdss&amp;redir_token=CbvFDWG2vMqEakSNLLwpLj19SdR8MTU1MzU0MzUzNUAxNTUzNDU3MTM1&amp;q=https%3A%2F%2Fblog.ubuntu.com%2F2018%2F12%2F27%2Ftop-snaps-in-2018&amp;event=video_description" target="_blank">https://blog.ubuntu.com/2018/12/27/to...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=1X3Md5GMdss&amp;redir_token=CbvFDWG2vMqEakSNLLwpLj19SdR8MTU1MzU0MzUzNUAxNTUzNDU3MTM1&amp;q=https%3A%2F%2Fwww.phoronix.com%2Fscan.php%3Fpage%3Dnews_item%26px%3DLinux-EOY-2018-Kernel-Stats&amp;event=video_description" target="_blank">https://www.phoronix.com/scan.php?pag...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=1X3Md5GMdss&amp;redir_token=CbvFDWG2vMqEakSNLLwpLj19SdR8MTU1MzU0MzUzNUAxNTUzNDU3MTM1&amp;q=https%3A%2F%2Fwww.polygon.com%2F2018%2F12%2F27%2F18158047%2Fbloodstained-cancels-mac-linux-apology-kickstarter-delay-2019&amp;event=video_description" target="_blank">https://www.polygon.com/2018/12/27/18...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=1X3Md5GMdss&amp;redir_token=CbvFDWG2vMqEakSNLLwpLj19SdR8MTU1MzU0MzUzNUAxNTUzNDU3MTM1&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fwindows-is-coming-to-your-chromebook%2F&amp;event=video_description" target="_blank">https://www.zdnet.com/article/windows...
