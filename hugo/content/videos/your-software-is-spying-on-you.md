---
title: "Your Software Is Spying On You" 
image: images/thumbs/0756.jpg
date: 2020-11-16T12:23:40+06:00
author: Derek Taylor
tags: ["FOSS Advocacy", "security"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/your-software-is-spying-on-you/ed283a0a8000955e47033e923985f6ab1954dfff?r=76mUtNJtYB9UMWjfP3uBjk7TneDBTrQe" allowfullscreen></iframe>

#### SHOW NOTES

In the last few days, one of the news headlines has been the revelation that MacOS is spying on its users and sending that information back to Apple. What can say? I'M SHOCKED! Ok, not shocked at all. But I wanted to talk a bit about surveillance (telemetry) in our software.
