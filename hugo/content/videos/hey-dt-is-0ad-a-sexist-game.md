---
title: "Hey, DT. Is 0 A.D. a Sexist Game? (Plus Other Questions)" 
image: images/thumbs/0779.jpg
date: 2020-12-24T12:23:40+06:00
author: Derek Taylor
tags: ["Hey DT", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/hey-dt-is-0-a-d-a-sexist-game-plus-other/2e20caccc85e9ddf44066082546e92cc4c5f3669?r=GSZ3gzTUm1Wd8E31jSHEfYy8jrVvUk2P" allowfullscreen></iframe>

#### SHOW NOTES

On this episode of "Hey DT", I respond to some questions that I've been getting.  The topics of these questions include: my hair style, my motivation for making Linux videos, the point of window manager hopping, anime, Evil Mode in Emacs and whether I think that the game 0 A.D is sexist?