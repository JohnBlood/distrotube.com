---
title: "Joplin Is An Open Source Alternative To Evernote"
image: images/thumbs/0541.jpg
date: 2020-02-11T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line", "Joplin", "GUI Apps"]
---

#### VIDEO

{{< amazon src="Joplin+Is+An+Open+Source+Alternative+To+Evernote.mp4" >}}
&nbsp;

#### SHOW NOTES

Are you looking for a free and open source note taking and todo application?  Do you need it to be cross-platform on Windows, Mac, Linux, Android and iOS?  Do you need it to sync to Dropbox, OneDrive and/or Nextcloud?  Look no further than Joplin!

REFERENCED:
+  https://joplinapp.org/
