---
title: "ArchBang Is The Original Easy To Install Arch Linux" 
image: images/thumbs/0751.jpg
date: 2020-11-09T12:23:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "ArchBang", "i3"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/archbang-is-the-original-easy-to-install/664d35a995b47c275ffe09f451dd031400d060ed?r=76mUtNJtYB9UMWjfP3uBjk7TneDBTrQe" allowfullscreen></iframe>

#### SHOW NOTES

ArchBang is one of the original easy-to-install Arch-based distributions. For a decade, the default window manager has been Openbox. The latest release of ArchBang marks a change though. Now, i3 is the default window manager.

REFERENCED:
+ https://archbang.org
