---
title: "Community Game Night - Xonotic"
image: images/thumbs/0586.jpg
date: 2020-04-02T12:23:40+06:00
author: Derek Taylor
tags: ["Live Stream", "Gaming"]
---

#### VIDEO

{{< amazon src="Community+Game+Night+-+Xonotic-p0MD4BGDyRs.mp4" >}}
&nbsp;

#### SHOW NOTES

Let's play some Xonotic, a fast paced first person shooter available for Linux!  Xonotic is free and open source software and is probably in your distro's repos, or you can install via: sudo snap install xonotic .  Want to chat with me in game and on stream?  Join the voice channel of my Discord.

REFERENCED:
+ https://xonotic.org/