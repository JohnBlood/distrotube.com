---
title: "A Preview Of Six Flavors Of Ubuntu 20.04 Focal Fossa"
image: images/thumbs/0604.jpg
date: 2020-04-23T12:23:40+06:00
author: Derek Taylor
tags: ["Ubuntu", "Kubuntu", "Xubuntu", "Lubuntu", "Ubuntu MATE", "Ubuntu Budgie"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-preview-of-six-flavors-of-ubuntu-20-04/b75b6d6b45064930d0771b9a0302b9f2db883e57?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this video, I take a quick look at the soon-to-be released versions of Ubuntu 20.04 codenamed "Focal Fossa."  I will briefly look at the flagship Ubuntu distribution as well as: Kubuntu, Xubuntu, Lubuntu, Ubuntu Budgie and Ubuntu MATE.

REFERENCED:
+ https://ubuntu.com/
+ https://kubuntu.org/
+ https://xubuntu.org/
+ https://lubuntu.me/
+ https://ubuntubudgie.org/
+ https://ubuntu-mate.org/
