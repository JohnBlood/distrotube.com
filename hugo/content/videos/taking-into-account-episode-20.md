---
title: "Taking Into Account, Ep. 20 - Windows Privacy, IRS Migration, Adobe on Linux, MS Linux"
image: images/thumbs/0313.jpg
date: Thu, 13 Dec 2018 19:23:25 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+20.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=PAYbd6k2h2w&amp;t=44s">0:44 Windows 10 recording and uploading your PC’s Activity History without you knowing. 

<a href="https://www.youtube.com/watch?v=PAYbd6k2h2w&amp;t=304s">5:04 IRS dropped the ball on Linux migration according to recent audit by Inspector General. 

<a href="https://www.youtube.com/watch?v=PAYbd6k2h2w&amp;t=531s">8:51 A comparison of twelve popular Linux DEs including LXDE, Pantheon and Trinity. 

<a href="https://www.youtube.com/watch?v=PAYbd6k2h2w&amp;t=1091s">18:11 Want Adobe's Premiere Pro ported over to Linux?  Let your voice be heard! 

<a href="https://www.youtube.com/watch?v=PAYbd6k2h2w&amp;t=1386s">23:06 MS-Linux?  Could Microsoft release a desktop Linux distro?  

<a href="https://www.youtube.com/watch?v=PAYbd6k2h2w&amp;t=1638s">27:18 I read a comment from a YouTube viewer regarding a recent video of mine. 

REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.techradar.com%2Fnews%2Fwindows-10-might-be-recording-and-uploading-your-pcs-activity-history-without-you-knowing&amp;redir_token=Xe7xen1-p1uVvKl_sKaa1yyx5YB8MTU1MzU0MTgzM0AxNTUzNDU1NDMz&amp;v=PAYbd6k2h2w&amp;event=video_description" target="_blank">https://www.techradar.com/news/window...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Ffcw.com%2Farticles%2F2018%2F12%2F11%2Firs-botched-linux-migration.aspx&amp;redir_token=Xe7xen1-p1uVvKl_sKaa1yyx5YB8MTU1MzU0MTgzM0AxNTUzNDU1NDMz&amp;v=PAYbd6k2h2w&amp;event=video_description" target="_blank">https://fcw.com/articles/2018/12/11/i...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.linuxlinks.com%2Flinux-desktop-environments-pantheon-trinity-lxde%2F&amp;redir_token=Xe7xen1-p1uVvKl_sKaa1yyx5YB8MTU1MzU0MTgzM0AxNTUzNDU1NDMz&amp;v=PAYbd6k2h2w&amp;event=video_description" target="_blank">https://www.linuxlinks.com/linux-desk...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2018%2F12%2F11%2Fwant-adobe-premiere-pro-ported-to-linux-heres-what-to-do%2F%233f693efe791e&amp;redir_token=Xe7xen1-p1uVvKl_sKaa1yyx5YB8MTU1MzU0MTgzM0AxNTUzNDU1NDMz&amp;v=PAYbd6k2h2w&amp;event=video_description" target="_blank">https://www.forbes.com/sites/jasoneva...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fadobe-video.uservoice.com%2Fforums%2F911233-premiere-pro%2Fsuggestions%2F36257581-yes-please-support-linux-this-would-be-a-huge-m&amp;redir_token=Xe7xen1-p1uVvKl_sKaa1yyx5YB8MTU1MzU0MTgzM0AxNTUzNDU1NDMz&amp;v=PAYbd6k2h2w&amp;event=video_description" target="_blank">https://adobe-video.uservoice.com/for...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fms-linux-lindows-could-microsoft-release-a-desktop-linux%2F&amp;redir_token=Xe7xen1-p1uVvKl_sKaa1yyx5YB8MTU1MzU0MTgzM0AxNTUzNDU1NDMz&amp;v=PAYbd6k2h2w&amp;event=video_description" target="_blank">https://www.zdnet.com/article/ms-linu...
