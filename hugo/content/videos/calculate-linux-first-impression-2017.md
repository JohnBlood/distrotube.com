---
title: "Calculate Linux First Impression Install & Review"
image: images/thumbs/0036.jpg
date: Sun, 19 Nov 2017 14:17:20 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Calculate Linux", "KDE"]
---

#### VIDEO

{{< amazon src="Calculate+Linux+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video review I look at Calculate Linux Desktop edition with the KDE desktop environment. It is a Gentoo-based distribution that is 100% compatible with Gentoo. It follows a rolling release model.
