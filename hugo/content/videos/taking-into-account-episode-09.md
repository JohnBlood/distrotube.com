---
title: "Taking Into Account, Ep 9 - Ubuntu Hyper-V, Open Source Contributors, ChromeOS, Windows 7, Blizzard"
image: images/thumbs/0287.jpg
date: Thu, 20 Sep 2018 19:40:24 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep+9+-+Ubuntu+Hyper-V%2C+Open+Source+Contributors%2C+ChromeOS%2C+Windows+7%2C+Blizzard.mp4" >}}
&nbsp;

#### SHOW NOTES

This weeks topics include: 

<a href="https://www.youtube.com/watch?v=1-sx296oBFY&amp;t=47s">0:47 Microsoft announces enhanced version of Ubuntu 18.04 for Hyper-V. 

<a href="https://www.youtube.com/watch?v=1-sx296oBFY&amp;t=394s">6:34 Who contributes the most to open source?  The results might surprise you. 

<a href="https://www.youtube.com/watch?v=1-sx296oBFY&amp;t=784s">13:04 Linux apps on your Chromebook?  It's now possible.

<a href="https://www.youtube.com/watch?v=1-sx296oBFY&amp;t=1144s">19:04 Still running Windows 7?  Time to switch---to Linux! 

<a href="https://www.youtube.com/watch?v=1-sx296oBFY&amp;t=1654s">27:34 Linux gamers playing Overwatch banned and then unbanned by Blizzard. 

<a href="https://www.youtube.com/watch?v=1-sx296oBFY&amp;t=1990s">33:10  I read a viewer question. 

REFERENCED IN THE VIDEO: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2018%2F09%2Fhyper-v-ubuntu-1804-windows-integration&amp;redir_token=wkvvOYFuTeYnTkdnIjTSl_K7N_V8MTU1MzQ1NjQxNUAxNTUzMzcwMDE1&amp;event=video_description&amp;v=1-sx296oBFY" target="_blank">https://www.omgubuntu.co.uk/2018/09/h..

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.techrepublic.com%2Farticle%2Fwho-contributes-most-to-open-source-the-answers-will-definitely-surprise-you%2F&amp;redir_token=wkvvOYFuTeYnTkdnIjTSl_K7N_V8MTU1MzQ1NjQxNUAxNTUzMzcwMDE1&amp;event=video_description&amp;v=1-sx296oBFY" target="_blank">https://www.techrepublic.com/article/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.infoworld.com%2Farticle%2F3253948%2Fopen-source-tools%2Fwho-really-contributes-to-open-source.html&amp;redir_token=wkvvOYFuTeYnTkdnIjTSl_K7N_V8MTU1MzQ1NjQxNUAxNTUzMzcwMDE1&amp;event=video_description&amp;v=1-sx296oBFY" target="_blank">https://www.infoworld.com/article/325...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fgizmodo.com%2Fhow-to-get-more-out-of-your-chromebook-by-running-linux-1829058899&amp;redir_token=wkvvOYFuTeYnTkdnIjTSl_K7N_V8MTU1MzQ1NjQxNUAxNTUzMzcwMDE1&amp;event=video_description&amp;v=1-sx296oBFY" target="_blank">https://gizmodo.com/how-to-get-more-o...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.ghacks.net%2F2018%2F09%2F17%2Fwindows-7-to-linux-preparations%2F&amp;redir_token=wkvvOYFuTeYnTkdnIjTSl_K7N_V8MTU1MzQ1NjQxNUAxNTUzMzcwMDE1&amp;event=video_description&amp;v=1-sx296oBFY" target="_blank">https://www.ghacks.net/2018/09/17/win...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2018%2F09%2F14%2Fcaution-playing-overwatch-on-linux-may-get-you-banned%2F%2339f6fa23259b&amp;redir_token=wkvvOYFuTeYnTkdnIjTSl_K7N_V8MTU1MzQ1NjQxNUAxNTUzMzcwMDE1&amp;event=video_description&amp;v=1-sx296oBFY" target="_blank">https://www.forbes.com/sites/jasoneva...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fdistrowatch.com%2F&amp;redir_token=wkvvOYFuTeYnTkdnIjTSl_K7N_V8MTU1MzQ1NjQxNUAxNTUzMzcwMDE1&amp;event=video_description&amp;v=1-sx296oBFY" target="_blank">https://distrowatch.com/
