---
title: "Scrot Is A Powerful Command Line Screenshot Utility"
image: images/thumbs/0492.jpg
date: 2020-01-05T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line", "dmenu"]
---

#### VIDEO

{{< amazon src="Scrot+Is+A+Powerful+Command+Line+Screenshot+Utility.mp4" >}}
&nbsp;

#### SHOW NOTES

Scrot is a minimalist command line screenshot utility that is very powerful due to its flexibility.  Partner scrot with a run launcher like dmenu or rofi for even more spectacular results


REFERENCED:
+ ► https://en.wikipedia.org/wiki/Scrot 
