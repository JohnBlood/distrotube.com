---
title: "ArchMerge First Impression Install & Review"
image: images/thumbs/0054.jpg
date: Fri, 08 Dec 2017 14:47:04 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArchMerge"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/archmerge-first-impression-install/10750173816a39b16ad34edc932d682e4a071308?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

ArchMerge is an Arch-based Linux distro that comes pre-installed with the XFCE desktop environment, the Openbox window manager, and the i3 tiling window manager. It is a 100% compatible with Arch. ArchMerge has since changed its name to Arco Linux.
