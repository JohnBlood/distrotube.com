---
title: "Taking Into Account, Ep. 19 - A.I., F-Bombs, Hugs, Necuno, Quora, Slate"
image: images/thumbs/0312.jpg
date: Wed, 05 Dec 2018 19:21:01 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep+19.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account:

 <a href="https://www.youtube.com/watch?v=XFjGRoQAjXg&amp;t=45s">0:45  What to expect with AI in 2019. 

<a href="https://www.youtube.com/watch?v=XFjGRoQAjXg&amp;t=456s">7:36 Linux patch replaces f-words with "hugs" in kernel comments. 

<a href="https://www.youtube.com/watch?v=XFjGRoQAjXg&amp;t=711s">11:51 A comparison of some Linux distros running on Amazon's EC2. 

<a href="https://www.youtube.com/watch?v=XFjGRoQAjXg&amp;t=962s">16:02 First truly open source smartphone?  Necuno unveils its KDE handset. 

<a href="https://www.youtube.com/watch?v=XFjGRoQAjXg&amp;t=1312s">21:52 Quora has been hacked.   Data of more than 100 million users stolen. 

<a href="https://www.youtube.com/watch?v=XFjGRoQAjXg&amp;t=1487s">24:47 I read a viewer question. 

REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=XFjGRoQAjXg&amp;redir_token=5WOjMqhwybaHkjGGvNOT_xJeV6F8MTU1MzU0MTY4MUAxNTUzNDU1Mjgx&amp;event=video_description&amp;q=https%3A%2F%2Fenterprisersproject.com%2Farticle%2F2018%2F12%2Fai-trends-2019" target="_blank">https://enterprisersproject.com/artic...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=XFjGRoQAjXg&amp;redir_token=5WOjMqhwybaHkjGGvNOT_xJeV6F8MTU1MzU0MTY4MUAxNTUzNDU1Mjgx&amp;event=video_description&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Flinux-patch-replaces-f-words-with-hugs-in-kernel-comments-but-some-cry-censorship%2F" target="_blank">https://www.zdnet.com/article/linux-p...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=XFjGRoQAjXg&amp;redir_token=5WOjMqhwybaHkjGGvNOT_xJeV6F8MTU1MzU0MTY4MUAxNTUzNDU1Mjgx&amp;event=video_description&amp;q=https%3A%2F%2Fwww.phoronix.com%2Fscan.php%3Fpage%3Darticle%26item%3Damazon-ec2-dec18%26num%3D1" target="_blank">https://www.phoronix.com/scan.php?pag...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=XFjGRoQAjXg&amp;redir_token=5WOjMqhwybaHkjGGvNOT_xJeV6F8MTU1MzU0MTY4MUAxNTUzNDU1Mjgx&amp;event=video_description&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Ffirst-truly-open-source-smartphone-necuno-unveils-its-kde-on-linux-handset%2F" target="_blank">https://www.zdnet.com/article/first-t...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=XFjGRoQAjXg&amp;redir_token=5WOjMqhwybaHkjGGvNOT_xJeV6F8MTU1MzU0MTY4MUAxNTUzNDU1Mjgx&amp;event=video_description&amp;q=https%3A%2F%2Ffossbytes.com%2Fquora-hacked-data-of-100-million-users-stolen%2F" target="_blank">https://fossbytes.com/quora-hacked-da...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=XFjGRoQAjXg&amp;redir_token=5WOjMqhwybaHkjGGvNOT_xJeV6F8MTU1MzU0MTY4MUAxNTUzNDU1Mjgx&amp;event=video_description&amp;q=https%3A%2F%2Fhelp.quora.com%2Fhc%2Fen-us%2Farticles%2F360020212652" target="_blank">https://help.quora.com/hc/en-us/artic...
