---
title: "Odio - Radio Streaming App for Windows, Mac and Linux"
image: images/thumbs/0329.jpg
date: Sat, 05 Jan 2019 19:57:19 +0000
author: Derek Taylor
tags: ["GUI Apps", ""]
---

#### VIDEO

{{< amazon src="Odio+Radio+Streaming+App+for+Windows%2C+Mac+and+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

Odio is a free (as in beer) radio streaming app that is available on 
Windows, Mac and Linux.  On Linux, you can install odio as a snap 
package with "sudo snap install odio".

<a href="https://www.youtube.com/redirect?event=video_description&amp;v=obIDoKD602o&amp;redir_token=77L34yQcDEEhxu2ETtErdbLC3iF8MTU1MzU0MzgzNkAxNTUzNDU3NDM2&amp;q=https%3A%2F%2Fodio.io%2F" rel="noreferrer noopener" target="_blank">https://odio.io/
