---
title: "Getting Started With Haskell"
image: images/thumbs/0637.jpg
date: 2020-06-01T12:23:40+06:00
author: Derek Taylor
tags: ["Programming", ""]
---

#### VIDEO

{{< amazon src="Getting+Started+With+Haskell.mp4" >}}
&nbsp;

#### SHOW NOTES

Some of the viewers have asked me to take a closer look at Haskell since I've done so much content regarding Xmonad, a tiling window manager written in Haskell.  Well, I'm not a programmer by trade.  And I certainly wouldn't say I "know" Haskell.  But I, a non-programmer, will attempt to cover some of the basics of Haskell in this video.  

REFERENCED:
+ http://learnyouahaskell.com/
+ https://www.haskell.org/