---
title: "If DT Made His Own OS, Things Would Be Radically Different!" 
image: images/thumbs/0740.jpg
date: 2020-10-24T12:23:40+06:00
author: Derek Taylor
tags: ["Vlog", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/if-dt-made-his-own-os-things-would-be/65d1ad46638538b0879d6a90d35ccfd030351e78?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

I often get asked by viewers, "If you made an OS, what would it look like?"  I will never make my own OS or my own Linux spin, because I'm not interested in being a support channel for people, nor do I have the time.  But if I were to play this out in my mind, here's what DT's OS would look like...