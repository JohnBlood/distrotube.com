---
title: "From Noob To Power User With Linux Mint Cinnamon"
image: images/thumbs/0631.jpg
date: 2020-05-25T12:23:40+06:00
author: Derek Taylor
tags: ["Linux Mint", "terminal"]
---

#### VIDEO

{{< amazon src="From+Noob+To+Power+User+With+Linux+Mint+Cinnamon.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy video, I go over some of the changes I would make in Linux Mint if I were using it as my daily driver.  I don't make drastic changes.  I just tweak some of the tools that are already there. 

REFERENCED:
+ https://www.linuxmint.com/