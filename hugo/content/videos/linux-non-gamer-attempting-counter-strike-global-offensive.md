---
title: "Linux Non-Gamer Attempting Counter Strike Global Offensive"
image: images/thumbs/0122.jpg
date: Fri, 23 Feb 2018 21:06:15 +0000
author: Derek Taylor
tags: ["Gaming", "CSGO"]
---

#### VIDEO

{{< amazon src="Linux+Non-Gamer+Attempting+Counter+Strike+Global+Offensive.mp4" >}}  
&nbsp;

#### SHOW NOTES

A quick video of me struggling to play Counter Strike: Global Offensive on Steam...on Linux. Not sure if I'm cut out for gaming. Definitely won't be trying my hand at competitive gaming any time soon.
