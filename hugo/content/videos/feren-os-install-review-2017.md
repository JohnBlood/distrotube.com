---
title: "Feren OS First Impression Install & Review"
image: images/thumbs/0013.jpg
date: Tue, 17 Oct 2017 01:52:59 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Feren OS"]
---

#### VIDEO

{{< amazon src="Feren+OS+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

I recently stumbled upon Feren OS which is a Linux distro that is based on Linux Mint and follows a pseudo rolling release model. Currently ranking well on Distrowatch.com, but I had never heard anything about this distribution, Let''s take a look!
