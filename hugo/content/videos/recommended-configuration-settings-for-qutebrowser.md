---
title: "Recommended Configuration Settings For Qutebrowser" 
image: images/thumbs/0734.jpg
date: 2020-10-16T12:23:40+06:00
author: Derek Taylor
tags: ["Qutebrowser", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/recommended-configuration-settings-for/e3d0c4653c404363a3381e75ce032ac6669fa179?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Here a few basic configuration settings that I add to my Qutebrowser config that I think make a lot of sense.  I often get viewers of the channel asking me about Qutebrowser, especially about adblocking, watching YouTube, and how to get a proper dark mode.   Here is how I do it.