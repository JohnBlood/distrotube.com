---
title: "ArchLabs 2018.02 Install & Review"
image: images/thumbs/0128.jpg
date: Wed, 28 Feb 2018 21:15:13 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArchLabs"]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/archlabs-2018-02-install-review/ce2c3e6640855db6f352d8438237c5d4d557e844?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

In this video I do a quick install and review of the latest release of ArchLabs, an Arch-based Linux distribution that uses the Openbox window manager. What's changed since the last time I reviewed ArchLabs? What do I like and what do I think could be improved? <a href="https://archlabslinux.com/2018/02/26/archlabs-first-release-for-2018/">https://archlabslinux.com/2018/02/26/archlabs-first-release-for-2018/</a>

