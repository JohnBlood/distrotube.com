---
title: "A Simple File Manager Using Dmenu"
image: images/thumbs/0471.jpg
date: Thu, 28 Nov 2019 00:20:00 +0000
author: Derek Taylor
tags: ["dmenu", "shell scripting", ""]
---

#### VIDEO

<iframe id="lbry-iframe" width="560" height="315" src="https://lbry.tv/$/embed/a-simple-file-manager-using-dmenu/3d37af5ed54613b2e067e9cca28d19cbbefb96e3?r=BEUHc6tbXzYauZBAtGsaAayhfjg8PRqR" allowfullscreen></iframe>

#### SHOW NOTES

Dmenufm is a simple file manager using dmenu.  It is a shell script that has very few dependencies and allows you to manage your files in a suckless sorta way.  Just another example of what dmenu can do!


#### REFERENCED:
+ https://github.com/huijunchen9260/dmenufm
