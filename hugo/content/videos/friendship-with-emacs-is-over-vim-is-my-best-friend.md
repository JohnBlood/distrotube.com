---
title: "Friendship With Emacs Is Over, Vim Is My Best Friend"
image: images/thumbs/0579.jpg
date: 2020-03-26T12:22:40+06:00
author: Derek Taylor
tags: ["Vim", "Emacs"]
---

#### VIDEO

{{< amazon src="Friendship+With+Emacs+Is+Over%2C+Vim+Is+My+Best+Friend.mp4" >}}
&nbsp;

#### SHOW NOTES

So I started using Emacs about six months ago because viewers of the channel were interested in how I (a Vim user) would get along with Emacs.  Well, I got along with it nicely.  But, I missed Vim.