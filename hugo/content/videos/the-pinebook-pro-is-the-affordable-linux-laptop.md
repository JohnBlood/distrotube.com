---
title: "The Pinebook Pro Is The Affordable Linux Laptop We've Been Waiting For" 
image: images/thumbs/0707.jpg
date: 2020-09-05T12:23:40+06:00
author: Derek Taylor
tags: ["Pinebook", "Hardware Reviews"]
---

#### VIDEO

{{< amazon src="The+Pinebook+Pro+Is+The+Affordable+Linux+Laptop+Weve+Been+Waiting+For.mp4" >}}
&nbsp;

#### SHOW NOTES

I ordered a Pinebook Pro a few weeks ago.  And as luck would have it, the laptop was delivered to me in the middle of dealing with Hurricane Laura and the power outage afterwards.  So it's been sitting in its box for a few days just waiting to be unleashed.  So here's an unboxing video.

REFERENCED:
+ https://www.pine64.org/pinebook-pro/