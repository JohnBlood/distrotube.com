---
title: "Linux Mint Debian Edition 'Cindy' Installation and First Look"
image: images/thumbs/0280.jpg
date: Tue, 11 Sep 2018 19:01:23 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Linux Mint"]
---

#### VIDEO

{{< amazon src="Linux+Mint+Debian+Edition+3+Cindy+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a quick look at Linux Mint Debian Edition 3 "Cindy".  
LMDE is based on Debian stable and features the Cinnamon desktop 
environment.  

<a href="https://www.youtube.com/redirect?q=https%3A%2F%2Flinuxmint.com%2Frel_cindy.php&amp;redir_token=ygCa2QrLdKwHIIuWeae3Z6W0frp8MTU1MzQ1NDA3M0AxNTUzMzY3Njcz&amp;v=qmt_lTLFtK8&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://linuxmint.com/rel_cindy.php
