---
title: "Getting PhpBB Installed and Themed"
date: 2020-01-01T12:22:40+06:00
image: images/blog/0003.jpg
author: Derek Taylor
---

### PhpBB Installation Documentation

Check the [PhpBB install documentation](https://www.phpbb.com/support/docs/) on how to install PhpBB on your computer.  


### Quick Install Method

1. Download phpBB3 from the website.

2. Decompress the phpBB3 archive to a local directory on your system.

3. Upload all the files contained in this archive (retaining the directory structure) to a web accessible directory on your server or hosting account.

4. Change the permissions on config.php to be writable by all (666 or -rw-rw-rw- within your FTP Client)
Change the permissions on the following directories to be writable by all (777 or -rwxrwxrwx within your FTP Client):
store/, cache/, files/ and images/avatars/upload/.

5. Point your web browser to the location where you uploaded the phpBB3 files with the addition of install/app.php or simply install/, e.g. http://www.example.com/phpBB3/install/app.php, http://www.example.com/forum/install/.

6. Click the INSTALL tab, follow the steps and fill out all the requested information.

7. Change the permissions on config.php to be writable only by yourself (644 or -rw-r--r-- within your FTP Client)


### Requirements
phpBB 3.2.x has a few requirements which must be met before you are able to install and use it.

+ A webserver or web hosting account running on any major Operating System with support for PHP.

+ A SQL database system, one of:
   + `MySQL 3.23 or above (MySQLi supported)`
   + `MariaDB 5.1 or above`
   + `PostgreSQL 8.3+`
   + `SQLite 3.6.15+`
   + `MS SQL Server 2000 or above (via ODBC or the native adapter)`
   + `Oracle`
   
&nbsp;
+ PHP 5.4.7+ but less than PHP 7.3 with support for the database you intend to use.

+ The following PHP modules are required: 
   + `json`
   + `getimagesize() function must be enabled.`
   
+ Presence of the following modules within PHP will provide access to additional features, but they are not required:
   + `zlib Compression support`
   + `Remote FTP support`
   + `XML support`
   + `GD Support`
